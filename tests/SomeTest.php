<?php

namespace tests;

use PHPUnit\Framework\TestCase;

class SomeTest extends TestCase
{
    public function test_obvious()
    {
        $this->assertEquals(true, '1' == 1);
        $this->assertEquals(false, '1' === 1);
    }
}